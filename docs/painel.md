# Painel

## Acessando o painel

Ao realizar login no Astecas, o usuário será redirecionado para seu painel. Caso o mesmo deseje acessar (em casos onde o mesmo está em outra página), o procedimento pode ser feito clicando em **painel** no menu de navegação do sistema.

<figure class="images">
    <img src="../assets/images/painel-acesso.JPG" />
</figure>

## Visão geral do painel

O **painel** visualizado pelo usuário será similar a este:

<figure class="images">
    <img src="../assets/images/painel-geral.JPG" />
</figure>

A seguir, serão explicadas as **diferentes áreas** do painel do cliente Astecas:

### Dados financeiros

A esqueda do painel, existe uma área que mostra um **resumo dos dados financeiros** da empresa selecionada. 

<figure class="images">
    <img src="../assets/images/painel-dados-lateral.JPG" />
</figure>

#### Detalhamento de dados

Alguns desses dados podem ser vistos de forma expandida através da opção **clique para detalhes**, realizando os seguintes passos:

1. Deslize o mouse sobre os dados, e, aqueles que possuirem a opção descrita anteriormente, poderão ser clicados.
<figure class="images">
    <img src="../assets/images/painel-dados-detalhes-clicar.jpg" />
</figure>

<ol start="2">
    <li>Ao serem clicados, é possível visualizar de forma completa as informações sobre o dado selecionado.</li>
</ol>
<figure class="images">
    <img src="../assets/images/painel-detalhes.JPG" />
</figure>

### Opções do painel

#### Seleção de ano e mês

Caso deseje, o usuário pode **selecionar outros perídos** a serem visualizados no painel da empresa. Para isso, basta clicar no menu e selecionar o perído desejado. É possível também, na área de busca, pesquisar o perído a ser visto.

<figure class="images">
    <img src="../assets/images/painel-selecionar-ano-mes.JPG" />
</figure>

#### Opções do painel

Se o usuário desejar, o mesmo pode **ocultar informações do painel** através desta opção. Para isso, existem duas opções:

* **Sim**: Ao clicar em "sim", o dado irá ser ocultado.
* **Não**: Ao clicar em "não", o dado será exibido.

<figure class="images">
    <img src="../assets/images/painel-opcoes.JPG" />
</figure>

#### Legenda

Por último, essa área mostra uma legenda que informa como os **números são mostrados no sistema**.

<figure class="images">
    <img src="../assets/images/painel-legenda.JPG" />
</figure>

### Gráficos e tabelas

#### Despesas

Na área de despesas, é mostrado um **gráfico e uma tabela** para visualização das despesas da empresa. Sendo assim, na área superior, é mostrado o gráfico relacionado aos diferentes tipos de despesas, e, na área inferior, são mostrados os tipos de despesas de forma extendida, com mais dados que dão suporte informacional.

<figure class="images">
    <img src="../assets/images/painel-despesas.JPG" />
</figure>

#### Evolução da Capacidade de Geração Líquida de Caixa

Nessa área é informado, através de gráfico, um **comparativo da evolução de geração líquida de caixa** entre um determinado período (eixo x) e o quantitativo (eixo y). 

<figure class="images">
    <img src="../assets/images/painel-evolucao.JPG" />
</figure>

#### Fluxo de Caixa

O **gráfico de fluxo de caixa** mostra um comparativo entre meses, considerando ingressos e desembolsos, mostrando o perído (eixo x) e o quantitativo (eixo y).

<figure class="images">
    <img src="../assets/images/painel-fluxo.JPG" />
</figure>

#### Geração de documentos

As opções de gráficos e tabelas apresentadas anteriormente, possuem um menu no seu canto superior direito que possibilita a **geração de documentos** em diversos formatos, contendo os dados especificados nos gráficos e tabelas. Para gerar quaisquer destes documentos, são necessários os seguintes passos:

1. Clicar no botão identificado por três pontos (...).
2. Selecionar a opção desejada.

<figure class="images">
    <img src="../assets/images/painel-gerar-documentos.JPG" />
</figure>

### Calendário financeiro

Por último, no painel, é mostrado o **calendário financeiro**, que corresponde aos ingressos e desembolsos realizados em um determinado dia do mês selecionado na [opção](#selecao-de-ano-e-mes) correspondente.

<figure class="images">
    <img src="../assets/images/painel-calendario.JPG" />
</figure>